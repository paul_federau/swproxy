function checkCommand(reqData, resData) {
    var { command } = reqData;
    
    if (command == 'BattleDungeonStart') {
        resData = modAtk(resData);                
    } else {
        console.log(command);
    };
    return resData;
};


function modAtk(data) {
    var { dungeon_unit_list } = data;
    console.log('\n----Response before modify----');
    console.log(dungeon_unit_list[0][0]);
    for (wave in dungeon_unit_list) {
        for (unit in dungeon_unit_list[wave]) {
            const u = dungeon_unit_list[wave][unit];
	        u.atk = 10;
        }
    };
    data.dungeon_unit_list = dungeon_unit_list;
    console.log('\n----Response after modify----');
    console.log(dungeon_unit_list[0][0]);
    return data;
};

module.exports = {
    checkCommand: (data1, data2) => checkCommand(data1, data2)
};
