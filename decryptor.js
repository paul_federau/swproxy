var crypto = require('crypto');
var zlib = require('zlib');
var encryptkey = require('./encryptkey');


var algorithm = 'aes-128-cbc';
var key = encryptkey.key();
var iv = '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00';


function encrypt(text) {
    var cipher = crypto.createCipheriv(algorithm, key, iv);
    var enc = cipher.update(text, 'latin1', 'base64');
    enc += cipher.final('base64');
    return enc
};

function decrypt(text) {
    var decipher = crypto.createDecipheriv(algorithm, key, iv);
    var dec = decipher.update(text, 'base64', 'latin1');
    dec += decipher.final('latin1');
    return dec;
};


module.exports = {
    dec_req: text => JSON.parse(decrypt(text)),
    dec_res: text => JSON.parse(zlib.inflateSync(Buffer.from(decrypt(text), 'latin1')).toString()),
    enc_res: text => encrypt(zlib.deflateSync(Buffer.from(JSON.stringify(text)), 'latin1'))
};