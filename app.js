var Proxy = require('http-mitm-proxy');
var url = require('url');
var net = require('net');
var { dec_req, dec_res, enc_res } = require('./decryptor');
var { checkCommand } = require('./modData');

proxy = Proxy();

proxy.onConnect(function(req, socket, head, callback) {
    var serverUrl = url.parse(`https://${req.url}`);
    if (req.url.includes('qpyou.cn')) {
        return callback();
    } else {
        var srvSocket = net.connect(serverUrl.port, serverUrl.hostname, () => {
            socket.write('HTTP/1.1 200 Connection Established\r\n' + 'Proxy-agent: Node-Proxy\r\n' + '\r\n');
            srvSocket.pipe(socket);
            socket.pipe(srvSocket);
        });
        srvSocket.on('error', () => {});
        socket.on('error', () => {});
    }
});

proxy.onRequest(function(ctx, callback) {
    if (ctx.clientToProxyRequest.url.includes('/api/gateway_c2.php')) {
        ctx.ReqChunk = [];
        ctx.ResChunk = [];
        
        ctx.onRequestData(function(ctx, chunk, callback) {
            ctx.ReqChunk.push(chunk);
            return callback(null, chunk);
        });

        ctx.onResponseData(function(ctx, chunk, callback) {
            ctx.ResChunk.push(chunk);
            return callback(null, null);
        });

        ctx.onResponseEnd(function(ctx, callback) {
            try {
                var reqData = dec_req(Buffer.concat(ctx.ReqChunk).toString());
                var resData = dec_res(Buffer.concat(ctx.ResChunk).toString());
                
                resData = checkCommand(reqData, resData);

                ctx.proxyToClientResponse.write(enc_res(resData));

                return callback();
            } catch (e) {
                return callback();
            }
        })
    };
    return callback();
});

proxy.listen({port:8080});